# README #
This project is an introduction to auto-reproducing (quine) programs.
There are three programs written in C: Colleen, Grace, and Sully.
The three programs are rewritten in javascript as a bonus.

### Colleen
This program includes:

* a main function
* two different commentaries
* an additional function

### Grace
This program writes into a file called Grace_kid.c

* no main declared
* three defines
* a comment

### Sully
This program writes to five consecutive files Sully_X.c where X increments
at each write.
