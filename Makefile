SRC1	= Colleen/Colleen.c
SRC2	= Grace/Grace.c
SRC3	= Sully/Sully.c

CC		= gcc
CFLAGS	= -Wall -Werror -Wextra

PROG1	= Colleen/Colleen
PROG2	= Grace/Grace
PROG3	= Sully/Sully

OBJ1	= $(SRC1:.c=.o)
OBJ2	= $(SRC2:.c=.o)
OBJ3	= $(SRC3:.c=.o)

all: $(PROG1) $(PROG2) $(PROG3)

$(PROG1): $(OBJ1)
		$(CC) $(CFLAGS) $(OBJ1) -o $(PROG1)

$(PROG2): $(OBJ2)
		$(CC) $(CFLAGS) $(OBJ2) -o $(PROG2)

$(PROG3): $(OBJ3)
		$(CC) $(CFLAGS) $(OBJ3) -o $(PROG3)

%.o: %.c
		$(CC) $(CFLAGS) $< -c -o $@

clean:
		rm -f $(OBJ1) $(OBJ2) $(OBJ3)
		rm -f Sully/Sully_*

fclean: clean
		rm -f $(PROG1) $(PROG2) $(PROG3)

re: fclean all
