function comment() {
    return '/*\n\tComment 1\n*/'
}
/*
	Comment 1
*/
function main() {
    /*
        Comment 2
    */
    console.log(comment.toString());
    console.log(comment());
    console.log(main.toString());
    console.log('main ();');
}
main ();
